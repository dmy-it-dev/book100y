<?php
require_once("library_things.php");
class LibraryBookClass extends LibraryThingsClass {
    function __construct() {
        parent::__construct();
    }

    public function sixBooksCard($sixBookdata,$caption){
        $html = "
        <section class='services1 cid-rzXj97Dew3' id='services1-6'>
            <div class='container'>
                <div class='row justify-content-center'>
                    <!--Titles-->
                    <div class='title pb-5 col-12'>
                        <h4 >
                            {$caption}
                        </h4>
                        
                    </div>
        
        ";

        foreach($sixBookdata as $key=>$value){
            $html.="{$this->thingsCard($sixBookdata[$key][NCODE])}";  
        }
   
        $html.="
                </div>
            </div>
        </section>
        ";
        return $html;
    }

    public function sixBooksCarousel($sixBookdata,$caption){
        $html = "
        <section class='services1 cid-rzXj97Dew3' id='services1-6'>
            <div class='container'>
                <div class='row justify-content-center'>
                    <!--Titles-->
                    <div class='title pb-5 col-12'>
                        <h4 >
                            {$caption}
                        </h4>
                        
                    </div>
                    <div class='carousel slide' role='listbox' data-pause='true' data-keyboard='false' data-ride='carousel' data-interval='5000'>
                    <div class='carousel-inner' data-visible='5'>
        
        ";

/*         <div class='container'>
        <div class='carousel slide' role='listbox' data-pause='true' data-keyboard='false' data-ride='carousel' data-interval='5000'>
            <div class='carousel-inner' data-visible='5'>
                <div class='carousel-item '>
                    <div class='media-container-row'>
                        <div class='col-md-12'>
                            <div class='wrap-img '>
                                <a href='https://library.msu.ac.th/th/' target=_blank' >
                                <img src='./images/suggested_web/msulib.png' class='img-responsive clients-img'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div> */
               // {$this->thingsCarosalItem($sixBookdata[$key][NCODE])}
        foreach($sixBookdata as $key=>$value){
            $html.="
            <div class='carousel-item '>
                <div class='media-container-row'>
                    <div class='col-md-12'>
                    <div class='wrap-img '>
                    <a href='https://library.msu.ac.th/th/' target=_blank' >
                        <img src='./images/suggested_web/msulib.png' class='img-responsive clients-img'>
                    </a>
                </div>
            
            
                    </div>
                </div>
            </div>
            ";  
        }
   
        $html.="
                        </div>
                    </div>
                </div>
            </div>
        </section>
        ";
        return $html;
    }

    public function suggestedBook($number){
        $sql = "select *from book_suggested limit {$number};";
        $data = $this->select($sql);

        return $this->sixBooksCard($data,'หนังสือแนะนำ');
    }

    public function newBook($number){
        $sql = "select *from book_new limit {$number};";
        $data = $this->select($sql);

        return $this->sixBooksCard($data,'หนังสือมาใหม่');
    }

    public function menuPageContent($mainMenu){
        $sql = "select *from book_menu where main_menu = '{$mainMenu}';";

        $menu_data = $this->select($sql);
        foreach($menu_data as $key=>$value){
            if($menu_data[$key][code][0] == 'B'){//หนังสือพุทธศาสนาใช้ 2 หลักในการแบ่งกลุ่ม
                $sql = "select DISTINCT(BNAME1),NCODE from book where left(`GROUP`,2) = left('{$menu_data[$key][code]}',2) AND POTO != '' limit 6;";
            }else if($mainMenu == 'อ้างอิง'){
                $sql = "select DISTINCT(BNAME1),NCODE 
                        from book where (left(`GROUP`,1) = left('{$menu_data[$key][code]}',1)  
                                        AND `GROUP`  != 'อนุสรณ์' AND `GROUP` != 'อน' AND POTO != '') OR (GCODE = 'อ') limit 6;";
            }else{
                $sql = "select DISTINCT(BNAME1),NCODE from book where left(`GROUP`,1) = left('{$menu_data[$key][code]}',1) AND POTO != '' limit 6;";
            }
            $books_data = $this->select($sql);

            if($menu_data[$key][code] != 'ย' and $menu_data[$key][code] != 'จ' and $menu_data[$key][code] != 'ร'){
                //$html .= $this->sixBooksCard($books_data,$menu_data[$key]['sub_menu'].' ('.$menu_data[$key][code].')');
                $html .= $this->sixBooksCard($books_data,$menu_data[$key]['sub_menu'].' ('.$menu_data[$key][code].')');
            }
            



        }
        return $html;
    }

}




?>