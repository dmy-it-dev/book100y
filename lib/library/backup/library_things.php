<?php

require_once("./lib/library/library_web.php");

class LibraryThingsClass extends LibraryWebClass {
    function __construct() {
        parent::__construct();
    }

	public function thingsCard($thingsId){
        $sql = "select *from book where NCODE = '{$thingsId}' limit 1;";

        $data = $this->select($sql);

        $coverImg = "./images/covers/no_cover.jpg";
        if (file_exists("./images/covers/{$data[0][NCODE]}.jpg")) {
            $coverImg = "./images/covers/{$data[0][NCODE]}.jpg";
        }

        $html = "
            
            <div class='card col-12 col-md-4 col-lg-2' id='{$data[0]['NCODE']}'>
                <a href='./detail.php?book_id={$data[0]['NCODE']}'>  
                <div class='card-wrapper'>
                    <div class='row'>
                    <div class='card-img col-12 equal'>
                        <img src='{$coverImg}' alt='{$data[0]['BNAME1']}'>
                    </div>
                    <div class='card-box col-12 mt-2'>
                        <h6 class='card-title mbr-fonts-style display-6'>
                            {$data[0]['BNAME1']}
                        </h6>
                </a>
                        <div class='row'>
                        <span class='mbr-text mbr-fonts-style col-lg-12'>
                            <a href='./search.php?wname={$data[0]['WNAME']}'>ผู้แต่ง: {$data[0]['WNAME']}</a>
                        </span>
                        <span class='mbr-text mbr-fonts-style col-lg-12'>
                            {$data[0]['GROUP']}
                        </span>
                        </div>
                    </div>
                    </div>
                </div>
                
            </div>

            <style>
            @media all and (min-width:768px){
                .equal{
                    height:252px;
                }
            }
            </style>
            
        ";
		return $html;
	}
	public function thingsDetailSection($thingsId){
        $sql = "select *from book where ncode = '{$thingsId}' limit 1;";
        $data = $this->select($sql);

        $html = "
            <div class='container'>
            <div class='card col-12 mt-2' id='{$data[0]['NCODE']}'>
                <div class='card-wrapper'>
                    <div class='row'>
                    <div class='card-img col-12 col-lg-4 equal mb-4'>

                        <img src='./images/covers/{$data[0][NCODE]}.jpg' alt='{$data[0]['BNAME1']}'>

                        <!--div class='col-lg-12 d-flex justify-content-center'>
                            <input type='button' value='จอง'>
                        </div-->
                    </div>
                    <div class='card-box col-sm-12 col-lg-8' >
                        <h4 class='card-title mbr-fonts-style display-5'>
                            ชื่อ: {$data[0]['BNAME1']}
                        </h4>
                        <p class='mbr-text mbr-fonts-style'>
                            <a href='./search.php?wname={$data[0]['WNAME']}'>ผู้แต่ง: {$data[0]['WNAME']}</a>
                        </p>
                        <p class='mbr-text mbr-fonts-style'>
                            <a href='./search.php?publi={$data[0]['PUBLI']}'>สำนักพิมพ์: {$data[0]['PUBLI']}</a>
                        </p>
                        <p class='mbr-text mbr-fonts-style'>
                            หมวด: {$data[0]['GROUP']}
                        </p>
                        <p class='mbr-text mbr-fonts-style'>
                            รหัสหนังสือ: {$data[0]['NCODE']}
                        </p>
                        <p class='mbr-text mbr-fonts-style'>
                            เลข ISBN: {$data[0]['ISBN']}
                        </p>
                        <p class='mbr-text mbr-fonts-style'>
                            จำนวนหน้า: {$data[0]['NPAGE']}
                        </p>
                        <p class='mbr-text mbr-fonts-style'>
                            <a href='./search.php?bhead={$data[0]['BHEAD']}'>หัวเรื่อง: {$data[0]['BHEAD']}</a>
                        </p>
                        <p class='mbr-text mbr-fonts-style'>
                            รายละเอียด: {$data[0]['SPECIAL1']}
                        </p>
                    </div>
                    </div>
                </div>
            </div>
            <p>
            </div>

        ";
		return $html;
	}
	public function thingsSearchResults($search_keyword,$wname,$publi,$bhead){
        $search_keyword = trim($search_keyword," ");
        $wname = trim($wname," ");
        $publi = trim($publi," ");
        if($publi != ''){
            $search_condition = " where PUBLI like '%{$publi}%' ";
        }else if($wname != ''){
            $search_condition = " where wname like '%{$wname}%' ";
        }else if($bhead != ''){
            $search_condition = " where bhead like '%{$bhead}%' ";        
        }else{
            $search_condition = " where bname1 like '%{$search_keyword}%' or wname like '%{$search_keyword}%' ";
        }

        $sql = "select DISTINCT(BNAME1),NCODE,WNAME,PUBLI,`GROUP`,BHEAD,book_lib_facebook,book_lib_name 
        from book inner join book_library on book.libcode = book_library.book_libcode
        {$search_condition}
        limit 50;";


        $data = $this->select($sql);

        $html = "
            <div class='container'>
            <p>";

        foreach($data as $key=>$value){

        $coverImg = "./images/covers/no_cover.jpg";
        if (file_exists("./images/covers/{$data[$key][NCODE]}.jpg")) {
            $coverImg = "./images/covers/{$data[$key][NCODE]}.jpg";
        }

        $html.="    
            <div class='card col-12 mt-1' id='{$data[$key]['NCODE']}'>
                <div class='card-wrapper row'>
                    <div class='card-img col-lg-2'>
                        <a href='./detail.php?book_id={$data[$key]['NCODE']}'> 
                            <div class='col-lg-12'>
                            <img src='{$coverImg}' alt='{$data[$key]['BNAME1']}'>
                            <p>
                            </div>
                            <!--div class='col-lg-12 d-flex justify-content-center'>
                                
                                <input type='button' value='จอง'>
                            </div-->
                        </a>
                    </div>
                    <div class='card-box col-lg-10' >
                        <div class='row'>
                            <h4 class='card-title mbr-fonts-style display-5 col-lg-12'>
                                <a href='./detail.php?book_id={$data[$key]['NCODE']}'>
                                ชื่อ: {$data[$key]['BNAME1']}
                                </a>
                            </h4>
                        </div>
                        <div class='row'>
                            <p class='mbr-text mbr-fonts-style col-lg-8'>
                                <a href='./search.php?wname={$data[$key]['WNAME']}'>ผู้แต่ง: {$data[$key]['WNAME']}</a>
                            </p>
                            <p class='mbr-text mbr-fonts-style col-lg-4'>
                                <a href='./search.php?bhead={$data[$key]['BHEAD']}'>หัวเรื่อง: {$data[$key]['BHEAD']}</a>
                            </p>
                        </div>
                        <div class='row'>
                            <p class='mbr-text mbr-fonts-style col-lg-4'>
                                <a href='./search.php?publi={$data[$key]['PUBLI']}'>สำนักพิมพ์: {$data[$key]['PUBLI']}</a>
                            </p>
                            <p class='mbr-text mbr-fonts-style col-lg-4'>
                                หมวด: {$data[$key]['GROUP']}
                            </p>
                            <p class='mbr-text mbr-fonts-style col-lg-4'>
                                รหัสหนังสือ: {$data[$key]['NCODE']}
                            </p>
                            <p class='mbr-text mbr-fonts-style col-lg-12'>
                                <a href='{$data[$key]['book_lib_facebook']}'>
                                ห้องสมุด: {$data[$key]['book_lib_name']}
                                </a>
                            </p>
                        
                        </div>
                    </div>
                </div>
                </a>
            </div>
        ";
        }
        $html.= "      <p>
                    </div>";
		return $html;
	}

}

?>