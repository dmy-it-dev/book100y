<?php 
require_once("./lib/library/library_base.php");

class LibraryWebClass extends LibraryBaseClass {
	var $conn;
	function __construct(){
		self::connect();
	}

	public function pageHeader($pageTitle){
        $html = "
        <head>
            <meta charset='UTF-8'>
            <meta http-equiv='X-UA-Compatible' content='IE=edge'>
            <meta name='generator' content='Mobirise v4.10.7, mobirise.com'>
            <meta name='viewport' content='width=device-width, initial-scale=1, minimum-scale=1'>
            <link rel='shortcut icon' href='./images/logo2.png?v=3' type='image/x-icon'>
            <meta name='description' content=''>
            
            <title>{$pageTitle}</title>
            <link rel='stylesheet' href='assets/web/assets/mobirise-icons/mobirise-icons.css'>
            <link rel='stylesheet' href='assets/tether/tether.min.css'>
            <link rel='stylesheet' href='assets/bootstrap/css/bootstrap.min.css'>
            <link rel='stylesheet' href='assets/bootstrap/css/bootstrap-grid.min.css'>
            <link rel='stylesheet' href='assets/bootstrap/css/bootstrap-reboot.min.css'>
            <link rel='stylesheet' href='assets/socicon/css/styles.css'>
            <link rel='stylesheet' href='assets/dropdown/css/style.css'>
            <link rel='stylesheet' href='assets/theme/css/style.css'>
            <link rel='stylesheet' href='assets/100ylib/css/additional.css?v=3' type='text/css'>
            
            <!-- Google Analytics -->
            <script async src='https://www.googletagmanager.com/gtag/js?id=UA-146905296-1'></script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
            
              gtag('config', 'UA-146905296-1');
            </script>
        
        </head>
        ";
		return $html;
    }
	public function pageNavbar($search_keyword){
        $html = "
        <section class='menu cid-qTkzRZLJNu' once='menu' id='menu1-0'>
        <nav class='navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm'>
            <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
                <div class='hamburger'>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </button>
            <div class='menu-logo'>
                <div class='navbar-brand'>
                    <span class='navbar-logo'>
                        <a href='./'>
                             <img src='./images/logo2.png?v=3' alt='Mobirise' style='height: 3.8rem;'>
                        </a>
                    </span>
                    <!--span class='navbar-caption-wrap col-12'>
                        <a class='navbar-caption text-white display-4' href='./'>
                            100yLibrary
                        </a>
                    </span-->
                </div>
            </div>
            <form class='form-inline' action='./search.php' method='get'>
                <div class='form-group has-search'>
                    <span class='mbri-search mbr-iconfont form-control-feedback pl-1' style='font-size: 1.8rem;'></span>
                    <input type='text' class='form-control' placeholder='ค้นหาชื่อหนังสือ ชื่อผู้แต่ง' style='width: 300px;'
                            value='{$search_keyword}'
                            name='keywords'
                    >
                </div>           
            </form>
            <div class='collapse navbar-collapse' id='navbarSupportedContent'>
                <ul class='navbar-nav nav-dropdown' data-app-modern-menu='true'>
                    <li class='nav-item'>
                        <a class='nav-link link text-white display-4' href='./'>
                            <!--span class='mbri-home mbr-iconfont mbr-iconfont-btn'></span-->
                            หน้าแรก
                        </a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link link text-white display-4' href='./menu.php?main_menu=หนังสือทั่วไป'>
                            <!--span class='mbri-home mbr-iconfont mbr-iconfont-btn'></span-->
                            หนังสือทั่วไป
                        </a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link link text-white display-4' href='./menu.php?main_menu=หนังสือพุทธศาสนา'>
                            หนังสือพุทธศาสนา
                        </a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link link text-white display-4' href='./menu.php?main_menu=อ้างอิง'>
                            อ้างอิง
                        </a>
                    </li>
                    <li class='nav-item'>
                        <a class='nav-link link text-white display-4' href='./menu.php?main_menu=อื่น ๆ'>
                            อื่นๆ
                        </a>
                    </li>
                    <!--li class='nav-item'>
                        <a class='nav-link link text-white display-4' href='./menu.php?main_menu=สื่อวีดีทัศน์'>
                            สื่อวีดีทัศน์
                        </a>
                    </li-->
                </ul>
                <!--div class='navbar-buttons mbr-section-btn'>
                    <a class='btn btn-sm btn-primary display-4' href='#'>
                        ลงชื่อเข้าใช้
                    </a>
                </div-->
            </div>
        </nav>
        </section>

        <style>
        .has-search .form-control {
            padding-left: 2.675rem;
        }
        
        .has-search .form-control-feedback {
            position: absolute;
            z-index: 2;
            display: block;
            width: 2.375rem;
            height: 2.375rem;
            line-height: 2.375rem;
            text-align: center;
            pointer-events: none;
            color: #aaa;
        }
        </style>

        ";
		return $html;
    }
    public function menuCard(){
        $html = "
        <div class='card mt-2 ml-3 border'>
            <div class='card-body'>
                <ul>
                    <li><a href='/library/web/member/menu.php?main_menu=หนังสือทั่วไป'><strong>หนังสือทั่วไป</strong></a></li>
                    <li><a href='/library/web/member/menu.php?main_menu=หนังสือพุทธศาสนา'><strong>หนังสือพุทธศาสนา</strong></a></li>
                    <li><a href='/library/web/member/menu.php?main_menu=อ้างอิง'><strong>อ้างอิงและวิทยานิพนธ์</strong></a></li>
                    <li><a href='/library/web/member/menu.php?main_menu=สื่อวีดีทัศน์'><strong>สื่อวีดีทัศน์</strong></a></li>
                </ul>
            </div>
        </div>

        <style>
        li {
            list-style:none;
            background-image:none;
            background-repeat:none;
            background-position:0; 
        }
        </style>
        ";
        return $html;
    }
    public function pageMenuSection(){
        $date = date('Y-m-d H:i:s');
        $html = "
        <section class='clients cid-big mt-5' data-interval='false' id='clients-9'>
            <div class='row'>
                <div class='col-md-3 pl-5'>
                    <h2>100Y LIBRARY</h2>
                    <h5>ห้องสมุดอาคารร้อยปีคุณยาย</h5>
                    <br>
                    <h5>เวลาเปิดทำการ</h5>
                    <span>วันจันทร์-วันเสาร์ (วันพฤหัสงดยืม)<br>
                    ปิดวันอาทิตย์ และ วันงานบุญใหญ่ของวัดพระธรรมกาย
                    </span>
                    <!--ul>
                        <li >เวลาเปิดทำการ</li>
                    </ul>
                        <li >ประวัติห้องสมุด</li>
                        <li >บุคคลากร</li>
                        <li >สิทธิ์การยืม</li>
                        
                        <li >ระเบียบการใช้ห้องสมุด</li>
                        <li >ข่าวประชาสัมพันธ์</li>
                    </ul-->
                </div>
                <div class='col-md-9'>
                    <div class='carousel slide mt-2' role='listbox' data-pause='true' data-keyboard='false' data-ride='carousel' data-interval='5000'>
                        <div class='carousel-inner' data-visible='1'>
                            <div class='carousel-item '>
                                <div class='media-container-row'>
                                    <div class='col-md-12'>
                                        <div class='wrap-img '>
                                            <img src='images/carousel/1.jpg?{$date}' class='img-responsive clients-img'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='carousel-item '>
                                <div class='media-container-row'>
                                    <div class='col-md-12'>
                                        <div class='wrap-img '>
                                            <img src='images/carousel/2.jpg?{$date}' class='img-responsive clients-img'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='carousel-item '>
                                <div class='media-container-row'>
                                    <div class='col-md-12'>
                                        <div class='wrap-img '>
                                            <img src='images/carousel/3.jpg?{$date}' class='img-responsive clients-img'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>        
        ";

        return $html;
    }

    public function pageSuggestedWebSection(){

        $html = "
        <section class='clients cid-rzXjDzgoAy' data-interval='false' id='clients-9'>
        <div class='container mb-5'>
            <div class='media-container-row'>
                <div class='col-12 align-center'>
                    <h2 >
                        เว็บแนะนำ
                    </h2>
                </div>
            </div>
        </div>

    <div class='container'>
        <div class='carousel slide' role='listbox' data-pause='true' data-keyboard='false' data-ride='carousel' data-interval='5000'>
            <div class='carousel-inner' data-visible='5'>
                <div class='carousel-item '>
                    <div class='media-container-row'>
                        <div class='col-md-12'>
                            <div class='wrap-img '>
                                <a href='https://library.msu.ac.th/th/' target=_blank' >
                                <img src='./images/suggested_web/msulib.png' class='img-responsive clients-img'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='carousel-item '>
                    <div class='media-container-row'>
                        <div class='col-md-12'>
                            <div class='wrap-img '>
                                <a href='https://www.nlt.go.th/th/' target=_blank'>
                                <img src='./images/suggested_web/nlt.png' class='img-responsive clients-img'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='carousel-item '>
                    <div class='media-container-row'>
                        <div class='col-md-12'>
                            <div class='wrap-img '>
                                <a href='http://www.tkpark.or.th/' target=_blank'>
                                <img src='./images/suggested_web/tkpark.png' class='img-responsive clients-img'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='carousel-item '>
                    <div class='media-container-row'>
                        <div class='col-md-12'>
                            <div class='wrap-img '>
                                <a href='https://www.maruey.com' target=_blank'>
                                <img src='./images/suggested_web/maruey.png' class='img-responsive clients-img'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='carousel-item '>
                    <div class='media-container-row'>
                        <div class='col-md-12'>
                            <div class='wrap-img '>
                                <a href='https://library.tu.ac.th/' target=_blank'>
                                <img src='./images/suggested_web/tulib.png' class='img-responsive clients-img'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='carousel-item '>
                    <div class='media-container-row'>
                        <div class='col-md-12'>
                            <div class='wrap-img '>
                                <a href='https://pariyat.com/' target=_blank'>
                                <img src='./images/suggested_web/pariyat.png' class='img-responsive clients-img'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='carousel-item '>
                    <div class='media-container-row'>
                        <div class='col-md-12'>
                            <div class='wrap-img '>
                                <a href='https://kalyanamitra.org/' target=_blank'>
                                <img src='./images/suggested_web/kalyanamitra.png' class='img-responsive clients-img'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='carousel-item '>
                    <div class='media-container-row'>
                        <div class='col-md-12'>
                            <div class='wrap-img '>
                                <a href='https://dmc.tv/' target=_blank'>
                                <img src='./images/suggested_web/dmctv.png' class='img-responsive clients-img'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='carousel-item '>
                    <div class='media-container-row'>
                        <div class='col-md-12'>
                            <div class='wrap-img '>
                                <a href='http://rsc.dhammakaya.network/dci/' target=_blank'>
                                <img src='./images/suggested_web/dcilib.png' class='img-responsive clients-img'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='carousel-item '>
                    <div class='media-container-row'>
                        <div class='col-md-12'>
                            <div class='wrap-img '>
                                <a href='http://th.dou.us/' target=_blank'>
                                <img src='./images/suggested_web/dou.png' class='img-responsive clients-img'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
 
            </div>
            <!--div class='carousel-controls'>
                <a data-app-prevent-settings='' class='carousel-control carousel-control-prev' role='button' data-slide='prev'>
                    <span aria-hidden='true' class='mbri-left mbr-iconfont'></span>
                    <span class='sr-only'>Previous</span>
                </a>
                <a data-app-prevent-settings='' class='carousel-control carousel-control-next' role='button' data-slide='next'>
                    <span aria-hidden='true' class='mbri-right mbr-iconfont'></span>
                    <span class='sr-only'>Next</span>
                </a>
            </div-->
        </div>
    </div>
</section>  

        ";
        return $html;

    }
	public function javascriptFooter($pageTitle){
        $html = "
        <script src='assets/web/assets/jquery/jquery.min.js'></script>
        <script src='assets/popper/popper.min.js'></script>
        <script src='assets/tether/tether.min.js'></script>
        <script src='assets/bootstrap/js/bootstrap.min.js'></script>
        <script src='assets/smoothscroll/smooth-scroll.js'></script>
        <script src='assets/ytplayer/jquery.mb.ytplayer.min.js'></script>
        <script src='assets/mbr-clients-slider/mbr-clients-slider.js'></script>
        <script src='assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js'></script>
        <script src='assets/dropdown/js/nav-dropdown.js'></script>
        <script src='assets/dropdown/js/navbar-dropdown.js'></script>
        <script src='assets/touchswipe/jquery.touch-swipe.min.js'></script>
        <script src='assets/vimeoplayer/jquery.mb.vimeo_player.js'></script>
        <script src='assets/theme/js/script.js'></script>
        <script src='assets/slidervideo/script.js'></script>
        ";
		return $html;
    }
    

    public function pageFooter(){

        $html = "
        <section once='footers' class='cid-rzXjsY7qGw' id='footer7-7'>
        <div class='container'>
            <div class='media-container-row align-center mbr-white'>
                <div class='row row-links'>
                    <ul class='foot-menu'>
                        <li class='foot-menu-item mbr-fonts-style display-7'>
                            <a class='text-white mbr-bold' href='#' target='_blank'>เกี่ยวกับเรา</a>
                        </li><li class='foot-menu-item mbr-fonts-style display-7'>
                            <a class='text-white mbr-bold' href='#' target='_blank'>ติดต่อ</a>
                        </li>
                    </ul>
                </div>
                <div class='row social-row'>
                    <div class='social-list align-right pb-2'>                    
                        <div class='soc-item'>
                            <a href='https://twitter.com/' target='_blank'>
                                <span class='socicon-twitter socicon mbr-iconfont mbr-iconfont-social'></span>
                            </a>
                        </div>
                        <div class='soc-item'>
                            <a href='https://www.facebook.com/book100y/' target='_blank'>
                                <span class='socicon-facebook socicon mbr-iconfont mbr-iconfont-social'></span>
                            </a>
                        </div>
                        <div class='soc-item'>
                            <a href='https://www.youtube.com/' target='_blank'>
                                <span class='socicon-youtube socicon mbr-iconfont mbr-iconfont-social'></span>
                            </a>
                        </div>
                        <!--div class='soc-item'>
                            <a href='https://instagram.com/mobirise' target='_blank'>
                                <span class='socicon-instagram socicon mbr-iconfont mbr-iconfont-social'></span>
                            </a>
                        </div><div class='soc-item'>
                            <a href='https://plus.google.com/u/0/+Mobirise' target='_blank'>
                                <span class='socicon-googleplus socicon mbr-iconfont mbr-iconfont-social'></span>
                            </a>
                        </div>
                        <div class='soc-item'>
                            <a href='https://www.behance.net/Mobirise' target='_blank'>
                                <span class='socicon-behance socicon mbr-iconfont mbr-iconfont-social'></span>
                            </a>
                        </div-->
                    </div>
                </div>
                <div class='row row-copirayt'>
                    <p class='mbr-text mb-0 mbr-fonts-style mbr-white align-center display-7'>
                        © Copyright 2019 Book100y - All Rights Reserved
                    </p>
                    <p>Powered by RGT</p>
                </div>
            </div>
        </div>
    </section>        
        
        ";

    return $html;

    }

}
?>