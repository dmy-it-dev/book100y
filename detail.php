<!DOCTYPE html>
<html>
    <?php
        require_once("./lib/library/library_things.php");
        $things = new LibraryThingsClass();

        echo $things->pageHeader("Detail");
    ?>
<body>
<?php 
        echo $things->pageNavbar('');   
?>
 <section class='clients cid-rzXjDzgoAy mt-1' data-interval='false' id='clients-9'>    
    <div class='row mt-5'>
        <div class='col-lg-12'>
            <?php echo $things->thingsDetailSection($_GET['book_id']); ?>
        </div>  
    </div>
</section>
<?php 

    //echo $things->pageSuggestedWebSection(); 
    echo $things->pageFooter(); 
    echo $things->javascriptFooter(); 

?>
</body>
</html>