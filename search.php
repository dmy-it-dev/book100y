<!DOCTYPE html>
<html>
    <?php
        require_once("./lib/library/library_things.php");
        $things = new LibraryThingsClass();

        echo $things->pageHeader("Detail");
    ?>
<body>
<?php 
        echo $things->pageNavbar($_GET['keywords']);   
?>
 <section class='clients mt-1' data-interval='false' id='clients-9' style='padding-top: 90px'>    
    <div class='row mt-5'>
        <!--div class='col-lg-3'>
            <?php echo $things->menuCard()?>
        </div-->
        <div class='col-lg-12'>
            <?php 

            echo $things->thingsSearchResults($_GET['keywords'],$_GET['wname'],$_GET['publi'],$_GET['bhead'],$_GET['gcode']); 
            ?>
        </div>  
    </div>
</section>
<?php 

    //echo $things->pageSuggestedWebSection(); 
    echo $things->pageFooter(); 
    echo $things->javascriptFooter(); 

?>
</body>
</html>