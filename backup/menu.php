<!DOCTYPE html>
<html>
    <?php
        require_once("./lib/library/library_things.php");
        require_once("./lib/library/library_book.php");
        $things = new LibraryThingsClass();
        $book = new LibraryBookClass();

        echo $things->pageHeader("Menu");
    ?>
<body>
<?php 
        echo $things->pageNavbar('');   
?>
 <section class='clients cid-rzXjDzgoAy mt-1' data-interval='false' id='clients-9'>
    <div class='container'>    
    <div class='row mt-5'>
        <div class='col-md-12'>
            <?php echo $book->menuPageContent($_GET['main_menu']); ?>
        </div>  
    </div>
    </div>
</section>
<?php 

    //echo $things->pageSuggestedWebSection(); 
    echo $things->pageFooter(); 
    echo $things->javascriptFooter(); 

?>
</body>
</html>