<!DOCTYPE html>
<html>
<?php

    require_once("./lib/library/library_things.php");
    require_once("./lib/library/library_book.php");
    
    $things = new LibraryThingsClass();
    $book = new LibraryBookClass();

    echo $things->pageHeader("Home");
?>
<body>
<?php 
    echo $things->pageNavbar('');
    echo $things->pageMenuSection();
    echo $book->newBook(6);
    echo $book->suggestedBook(6);
    echo $things->pageSuggestedWebSection(); 
    
    echo $things->pageFooter(); 
    echo $things->javascriptFooter(); 

?>
</body>
</html>