<!DOCTYPE html>
<html>
    <?php
        require_once("./lib/library/library_web.php");
        require_once("./lib/library/library_book.php");
        $web = new LibraryWebClass();
        $book = new LibraryBookClass();

        echo $web->pageHeader("Menu");
    ?>
<body>
<?php 
        echo $web->pageNavbar('');   
?>
    <div class='row mt-5'>
        <div class='col-md-12'>
            <?php echo $book->menuPageContent($_GET['main_menu']); ?>
        </div>  
    </div>
<?php 

    //echo $things->pageSuggestedWebSection(); 
    echo $web->pageFooter(); 
    echo $web->javascriptFooter(); 

?>
</body>
</html>